const fs = require("fs");
const { parse } = require("csv-parse");
const request = require('request');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const { uuid } = require('uuidv4');

fs.createReadStream("./enderecos.csv")
  .pipe(parse({ delimiter: ",", from_line: 1 }))
  .on("data", function (row) {
    endereco = (row[0]+","+row[1]+","+row[2]+","+row[3]);
    codificarEndereco(endereco);
  })


  let ACCESS_TOKEN = 'pk.eyJ1IjoiZ3VzdGF2b21vcmFpcyIsImEiOiJjbDc5YWFxdXcwZzh6NDBzajF4ZGZ0dnltIn0.cb4l2jZtUAkefRYrfb41-g';
  const codificarEndereco = function (endereco) {
      let url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/'
          + encodeURIComponent(endereco) + '.json?access_token='
          + ACCESS_TOKEN + '&limit=1';
  
      request({ url: url, json: true }, (err, response)=>{
          if (err) {
              callback('Erro ao conectar com a API', undefined);
          } else if (response.body.features.length == 0) {
              callback('Localização não encontrada');
          } else {
              var longitude = response.body.features[0].center[0];
              var latitude = response.body.features[0].center[1];
              var location = response.body.features[0].place_name;
              const csvWriter = createCsvWriter({
                  append: true,
                  path: 'enderecosGeocodificados.csv',
                  header: [
                      { id: 'Id_gerado', title: 'Id gerado:' },
                      { id: 'Latitude', title: 'Latitude' },
                      { id: 'Longitude', title: 'Longitude' },
                      { id: 'Location', title: 'Location' }
                  ]
              });
  
              let data = [
                  {
                      Id_gerado: uuid(),
                      Latitude: latitude,
                      Longitude: longitude,
                      Location: location
                  }
              ]
  
              csvWriter
                  .writeRecords(data)
                  .then(() => console.log('Endereço codificado com sucesso!'));
  
          }
      })
  }